<?php

/**
 * @file
 * Tokens for x_reference module.
 */

/**
 * Implements hook_token_info().
 */
function x_reference_token_info() {
  $fetched_type = 'x_reference_relation_fetched';

  $types[$fetched_type] = array(
    'name' => t('relation_fetched'),
    'description' => t("Fetched relation."),
  );

  $fetched['entity_id'] = array(
    'name' => t('Entity id'),
    'description' => t('Related item id.'),
  );
  $fetched['entity_type'] = array(
    'name' => t('Entity type'),
    'description' => t('Related entity type.'),
  );
  $fetched['source_name'] = array(
    'name' => t('Source'),
    'description' => t('Related item source.'),
  );

  return array(
    'types' => $types,
    'tokens' => array(
      $fetched_type => $fetched,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function x_reference_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // Do not handle any other types of tokens.
  if (strpos($type, 'x_reference') === 0) {
    $data = $data[$type];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'entity_id':
        case 'entity_type':
        case 'source_name':
          $replacements[$original] = $data[$name];
          break;
      }
    }
  }

  return $replacements;
}
