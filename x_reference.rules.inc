<?php

/**
 * @file
 * Rules integration for x_reference module.
 */

/**
 * Implements hook_rules_action_info().
 */
function x_reference_rules_action_info() {
  $load_references = array(
    'label' => t('Load related entities.'),
    'group' => t('x_reference'),
    'named parameter' => TRUE,
    'base' => 'x_reference_action_relation_fetch',
    'parameter' => array(
      'source_name' => array(
        'label' => t('Item source'),
        'type' => 'text',
      ),
      'entity_type' => array(
        'label' => t('Entity type'),
        'description' => t('Use token.'),
        'type' => 'text',
      ),
      'entity_id' => array(
        'label' => t('entity id.'),
        'type' => 'text',
      ),
      'trello_board' => array(
        'label' => t('Trello board token .'),
        'type' => 'text',
        'optional' => TRUE,
      ),
      'related_source' => array(
        'label' => t('Related item source'),
        'type' => 'text',
        'options list' => 'x_reference_get_unique_sources',
      ),
    ),
    'provides' => array(
      'x_reference_relation_fetched' => array(
        'type' => 'x_reference_relation_fetched',
        'label' => t('Fetched relation'),
      ),
    ),
  );

  $set_references = array(
    'label' => t('Set reference.'),
    'group' => t('x_reference'),
    'named parameter' => TRUE,
    'base' => 'x_reference_action_set_reference',
    'parameter' => array(
      'source_name_left' => array(
        'label' => t('Left item source'),
        'type' => 'text',
      ),
      'entity_type_left' => array(
        'label' => t('Left item type'),
        'description' => t('Use token.'),
        'type' => 'text',
      ),
      'entity_id_left' => array(
        'label' => t('Left entity id.'),
        'type' => 'text',
      ),
      'source_name_right' => array(
        'label' => t('Right item source'),
        'type' => 'text',
      ),
      'entity_type_right' => array(
        'label' => t('Right entity type'),
        'description' => t('Use token.'),
        'type' => 'text',
      ),
      'entity_id_right' => array(
        'label' => t('Right entity id.'),
        'type' => 'text',
      ),
    ),
  );

  return array(
    'load_references' => $load_references,
    'set_references' => $set_references,
  );
}

/**
 * Action: Fetch data.
 */
function x_reference_action_relation_fetch($params) {
  $left_item = array(
    'source_name' => $params['source_name'],
    'entity_type' => $params['entity_type'],
    'entity_id' => $params['entity_id'],
  );

  $right_item = array(
    'source_name' => $params['related_source'],
  );

  $relations = x_reference_get_references($left_item, $right_item);
  if (!$relations) {
    throw new RulesEvaluationException('Unable to load relation @source @entity with id "@id"', array(
        '@id' => $params['entity_id'],
        '@entity' => $params['entity_type'],
        '@source' => $params['source_name'],
      )
    );
  }

  return $relations[0] ? array('x_reference_relation_fetched' => $relations[0]) : array();
}

/**
 * Action: set reference.
 */
function x_reference_action_set_reference($params) {
  $left_item = array(
    'source_name' => $params['source_name_left'],
    'entity_type' => $params['entity_type_left'],
    'entity_id' => $params['entity_id_left'],
  );
  $right_item = array(
    'source_name' => $params['source_name_right'],
    'entity_type' => $params['entity_type_right'],
    'entity_id' => $params['entity_id_right'],
  );
  x_reference_set_reference($left_item, $right_item);
}
